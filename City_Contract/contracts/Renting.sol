pragma solidity 0.5.16;
pragma experimental ABIEncoderV2;
import "../../Master_Contract/contracts/Master.sol";

contract Renting{

     struct Bike{
       address owner;
       address bike_account;
       uint hirer_refund;
       uint base_price;
       uint cpkm; // costs per km
       // 0 indicates that this bike is not hired yet
       address hirer;
       uint amount_hirer_paid;
       uint lat;
       uint lon;
       bool started_return;
       uint renting_time;
    }

    struct bikeIdler{
        bool exists;
        uint id;
    }

    mapping(uint => Bike) public bikes;
    mapping(address => uint) public refunds;
    
    uint public bikeCount;

    // TODO: find a "good" value or add it to the constructor
    uint public constant owner_refund = 42;
    
    //events
    event addBikeEvent(
       address bikeAccount,
       uint bikeId,
       uint lat,
       uint lon
    );

    event rentBikeEvent(
      address bikeAccount 
    );

    event returnBikeEvent(
      address bikeAccount
    );

    event removeBikeEvent(
      address bikeAccount
    );

    event refundEvent(
      address account
    );

    event correctLocationEvent(
      uint bikeId
    );

    event bikeIdEvent(
      address bike,
      bool exists,
      uint bikeId
    );

    event bikeStateEvent(
      address bike,
      bool available,
      bool started_return,
      uint lat,
      uint lon
    );
    
    // Constructor
    constructor(string memory _country, string memory _state, string memory _city, address _master_address) public{

	Master m = Master(_master_address);
	m.register(_country, _state, _city, address(this));
	
    }

//    function getBikeState(uint bikeId) external {
//      require(bikes[bikeId].bike_account == msg.sender);
//      emit bikeStateEvent(bikes[bikeId].bike_account, (bikes[bikeId].hirer == address(0)), bikes[bikeId].started_return, bikes[bikeId].lat,  bikes[bikeId].lon);
//    }

    function getBikeState(uint bikeId) external view returns(Bike memory) {
        return bikes[bikeId];
    }

    function getMyBikeId() external view returns(bikeIdler memory) {
      bool exists = false;
      uint id = 0;
      for(uint i = 1; i < bikeCount+1; i++) {
    	if(bikes[i].bike_account == msg.sender) {
	        exists = true;
	        id = i;
	        i = bikeCount;
	    }
      }
      
      bikeIdler memory result;
      result.exists = exists;
      result.id = id;
      return result;
    }
    
    function bikeLockClosed(uint _bikeId, uint _lat, uint _lon, uint _km_travelled) external{
      require(bikes[_bikeId].bike_account == msg.sender, "this is not the correct bike");

      if(bikes[_bikeId].renting_time >= now){
        bikes[_bikeId].hirer = address(0);
        bikes[_bikeId].lat = _lat;
        bikes[_bikeId].lon = _lon;

        bikes[_bikeId].started_return = false;
        uint fund;
        uint owner_fund;
        if (bikes[_bikeId].amount_hirer_paid < (bikes[_bikeId].base_price + _km_travelled*bikes[_bikeId].cpkm)){
          fund = 0;
          owner_fund = bikes[_bikeId].amount_hirer_paid;
        } else {
          fund = bikes[_bikeId].amount_hirer_paid - bikes[_bikeId].base_price - _km_travelled*bikes[_bikeId].cpkm;
          owner_fund = bikes[_bikeId].base_price + _km_travelled*bikes[_bikeId].cpkm;
        }
        bikes[_bikeId].amount_hirer_paid = 0;
        
        refunds[bikes[_bikeId].owner] += owner_fund;
        refunds[bikes[_bikeId].hirer] += fund;
      } else {
        bikes[_bikeId].hirer = address(0);
        bikes[_bikeId].renting_time = 0;
        bikes[_bikeId].lat = _lat;
        bikes[_bikeId].lon = _lon;
        bikes[_bikeId].amount_hirer_paid = 0;
      }
    }

    function correctLocation(uint _bikeId, uint _lat, uint _lon) external{
      require(msg.sender == bikes[_bikeId].bike_account, "Wrong bike account");
      bikes[_bikeId].lat = _lat;
      bikes[_bikeId].lon = _lon;
      emit correctLocationEvent(_bikeId);
    }
    
    //adds bike to the bikes mapping with the specified costs
    function addBike(uint _base_price, uint _cpkm, uint _hirer_refund, address _bikeAccount, uint _lat, uint _lon) external payable{
      require(msg.value >=  owner_refund);
      bikeCount++;
      uint bikeId = bikeCount;
      bikes[bikeId] = Bike(msg.sender, _bikeAccount, _hirer_refund, _base_price, _cpkm, address(0), 0, _lat, _lon, false, 0);
      emit addBikeEvent(_bikeAccount, bikeId, _lat, _lon); 
    }


    function rentBike(uint _bikeId) external payable{
      require(bikes[_bikeId].owner != address(0), "Bike-ID not valid");
      require(bikes[_bikeId].hirer == address(0), "Bike is already rented");
      require(bikes[_bikeId].hirer_refund + bikes[_bikeId].base_price <= msg.value + refunds[msg.sender],  "Refund to low");

      refunds[msg.sender] += msg.value;
      refunds[msg.sender] -= bikes[_bikeId].hirer_refund + bikes[_bikeId].base_price;
      bikes[_bikeId].amount_hirer_paid = bikes[_bikeId].hirer_refund + bikes[_bikeId].base_price;
      bikes[_bikeId].hirer = msg.sender;
      bikes[_bikeId].renting_time = now + 1 days;
      
      emit rentBikeEvent(bikes[_bikeId].bike_account); 
    }

    function returnBike(uint _bikeId) external{
      require(bikes[_bikeId].owner != address(0), "Bike-ID not valid");
      require(bikes[_bikeId].hirer == msg.sender, "You are not the renter of the bike");

      bikes[_bikeId].started_return = true;
      
      emit returnBikeEvent(bikes[_bikeId].bike_account);
    }

    function removeBike(uint _bikeId) external{
      require(bikes[_bikeId].owner != address(0), "Bike-ID not valid");
      require(bikes[_bikeId].hirer == address(0), "Bike is rented");
      require(bikes[_bikeId].owner == msg.sender, "You are not the owner of the bike");
      refunds[bikes[_bikeId].owner] += owner_refund;
      delete bikes[_bikeId];
      emit removeBikeEvent(bikes[_bikeId].bike_account);
    }

    function payIn() external payable {
      refunds[msg.sender] += msg.value;
    }

    // if amount == 0 all eth is sent back
    function refund(uint amount) external{
      require(amount <= refunds[msg.sender], "you cannot withdraw more eth than you have");
      if (amount == 0) {
	amount = refunds[msg.sender];
      }
      refunds[msg.sender] -= amount;
      emit refundEvent(msg.sender); 
      msg.sender.transfer(amount);
    }
}

