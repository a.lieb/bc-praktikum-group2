var Renting = artifacts.require("./Renting.sol");

var address =  "0x8Cd4F97d4277c0Cc9F3FdbbcD5ACb897FF2f5bCc";


module.exports = function(deployer) {
    deployer.deploy(Renting, "Germany", "Hessen", "Darmstadt", address);
    deployer.deploy(Renting, "Germany", "Hessen", "Frankfurt", address);
};
