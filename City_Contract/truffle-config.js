var HDWalletProvider = require("truffle-hdwallet-provider");
var mnemonic = "mnemonic ..."

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  networks: {
    rinkeby:{
      provider: function() {
        return new HDWalletProvider(mnemonic, "https://rinkeby.infura.io/v3/0358be8911f346c4a460ea0adf564214");
      },
      network_id: "*"
    }
   /*  development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*" // Match any network id
    },
    develop: {
      port: 8545
    }*/
  } 
};
