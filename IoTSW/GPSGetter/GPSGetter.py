import os
import struct
from threading import Thread
import time
from serial import Serial
import logging

class gpsdata:
    def __init__(self, time, n, e):
        self.time = time
        self.n = n
        self.e = e

    def get_time(self):
        return self.time

    def get_n(self):
        return self.n

    def get_e(self):
        return self.e

class gpsgetter:
    
    def helper(loself):
        ser=Serial('/dev/ttyS0')
        ser.flushInput()
        while True:
            try:
                bytes = ser.readline()
                text = bytes.decode("utf-8")
                if (text.find("GPGGA") >= 0):
                    arr = text.split(',')
                    loself.set_values(gpsdata(arr[1], arr[2], arr[4]))
                    time.sleep(30)
                    ser.flushInput()
            except Exception as ex:
                logging.warning(ex)
                logging.warning('continue anyway')
            
    def start_getter(self):
        self.actual = gpsdata(-1, -1, -1)
        self.runner = Thread(target=gpsgetter.helper, args=[self])
        self.runner.start()

    def get_values(self):
        while(len(self.actual.get_n()) == 0 or self.actual.get_n()[0] == '-'):
                time.sleep(10)
        return self.actual

    def set_values(self, new):
        self.actual = new
