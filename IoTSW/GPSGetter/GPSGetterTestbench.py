import GPSGetter
import time

gps = GPSGetter.gpsgetter()

gps.start_getter()

while True:
    value = gps.get_values()
    print("t: " + str(value.get_time()))
    print("n: " + str(value.get_n()))
    print("e: " + str(value.get_e()))
    time.sleep(10)
