from time import sleep
import RPi.GPIO as GPIO
import logging

class Bikelock:
        def __init__(self):
                GPIO.setmode(GPIO.BCM)
                self.runner_one = 3;
                self.runner_two = 3;
                self.pins = [4, 17, 23, 24]
                self.time = 0.001
                self.steps = 1000
                self.filename = '/home/pi/IoTSW/lock/locked.txt'

                for i in range(4):
                        GPIO.setup(self.pins[i], GPIO.OUT)
                        GPIO.output(self.pins[i], False)

        def _helper_step(self, pin1, pin2, enable):
                GPIO.output(pin1, enable)
                if(pin1 != pin2):
                        GPIO.output(pin2, enable)

        def _change_runner(self, decrease_runner):
                if(self.runner_one == self.runner_two):
                        self.runner_one -= (1 if decrease_runner else -1)
                        self.runner_one %= 4
                else:
                        self.runner_two = self.runner_one

        def _step(self, decrease_runner):
                self._helper_step(self.pins[self.runner_one], self.pins[self.runner_two], True)
                sleep(self.time)
                self._helper_step(self.pins[self.runner_one], self.pins[self.runner_two], False)
                self._change_runner(decrease_runner)

        def close(self):
                f = open(self.filename, "r")
                if (f.read()[0:2] == "un"):
                        logging.info("close")
                        self.runner_one = 3
                        self.runner_two = 3
                        for i in range(self.steps):
                                self._step(False)
                        f.close()
                        f = open(self.filename, "w")
                        f.write("locked")
                        f.close()

        def open(self):
                f = open(self.filename, "r")
                if not(f.read()[0:2] == "un"):
                        logging.info("open")
                        self.runner_two = 3
                        self.runner_one = 0
                        for i in range(self.steps):
                                self._step(True)
                        f.close()
                        f = open(self.filename, "w")
                        f.write("unlocked")
                        f.close()

