import rinkeby_connector
import logging
import datetime
import sys


now = datetime.datetime.now()
logfilename = 'home/pi/' + str(now).replace(' ', '_') + '.log'
logging.basicConfig(filename=logfilename,level=logging.INFO)


f = open("/home/pi/IoTSW/Priv_Key.txt", "r")

try:
        #address city contract darmstadt: 0xaf166648e3eb2ae4779a90e95f7ea4472b085105
        connection = rinkeby_connector.rinkeby_connector(
                                f.read()[0:66], 
                                "wss://rinkeby.infura.io/ws/v3/0358be8911f346c4a460ea0adf564214",
                                '0xc981b8473fa54c5928c8015f0c46264a96d88735'
                                )
        connection.start_watching_for_events(5)
except:
        logging.error("Unexpected error:", sys.exc_info()[0])
