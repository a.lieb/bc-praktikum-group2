from web3 import Web3, HTTPProvider
import json
from web3.middleware import geth_poa_middleware
import pprint
import time
from geopy.distance import geodesic
import sys
from threading import Thread
import logging

sys.path.append(os.path.join(sys.path[0], 'GPSGetter'))
import GPSGetter

sys.path.append(os.path.join(sys.path[0], 'lock'))
import Lock

class rinkeby_connector:

        def _distance_measure(self):
                last = self.gpsgetter.get_values()
                self.distance = 0
                while(self.running):
                        actual = self.gpsgetter.get_values()
                        last_place = (float(last.get_n())/100, float(last.get_e())/100)
                        actual_place = (float(actual.get_n())/100, float(actual.get_e())/100)
                        if(actual.get_time() != last.get_time()):
                                self.distance += geodesic(last_place, actual_place).km
                        last = actual
                        time.sleep(30)

        def __init__(self, private_key, provider, city_contract_address):
                logging.info('start creating bikelock')
                self.lock = Lock.Bikelock()
                logging.info('bike lock created')
                self.distance = 0
                logging.info('start creating gpsgetter')
                self.gpsgetter = GPSGetter.gpsgetter()
                self.gpsgetter.start_getter()
                logging.info('gpsgetter created')
                self.bikeId = -1
                self.no_of_decimal_places_using_gps = 6
                self.private_key = private_key
                logging.info('start web3 stuff')
                self.w3 = Web3(Web3.WebsocketProvider(provider))
                self.w3.middleware_onion.inject(geth_poa_middleware, layer=0) # Rinkeby ONLY
                self.account = self.w3.eth.account.from_key(private_key)
                # parsing City Contract ABI
                with open("/home/pi/IoTSW/Renting.json") as renting:
                        info_json = json.load(renting)
                self.renting = self.w3.eth.contract(address = Web3.toChecksumAddress(city_contract_address), abi = info_json["abi"])
                logging.info('web3 connection done')
                self._get_bike_id()
                logging.info('constructor done')

        def _send_bike_position(self):
                val = self.gpsgetter.get_values()
                lat = int(float(val.get_n())*(10 ** (self.no_of_decimal_places_using_gps-2)))
                lon = int(float(val.get_e())*(10 ** (self.no_of_decimal_places_using_gps-2)))
                tx = self.renting.functions.correctLocation(self.bikeId, lat, lon).buildTransaction({
                        'from': self.account._address,
                        'gas': 70000,
                        'nonce': self.w3.eth.getTransactionCount(self.account._address)
                })
                signed_tx = self.w3.eth.account.sign_transaction(tx, private_key=self.private_key)
                self.w3.eth.sendRawTransaction(signed_tx.rawTransaction)
                logging.info('bike position sent')

        def _check_position(self, block_lat, block_lon):
                print('start check position, given position is %u %u', block_lat, block_lon)
                val = self.gpsgetter.get_values()
                block_place = (block_lat/(10 ** self.no_of_decimal_places_using_gps), block_lon/(10 ** self.no_of_decimal_places_using_gps))
                # the division by 100 is needed due to the return format of the NEO-6M
                actual_place = (float(val.get_n())/100, float(val.get_e())/100)
                if(geodesic(block_place, actual_place).km > 0.5):
                        self._send_bike_position()
                else:
                        logging.info("send not neccessary")

        def _get_bike_status(self):
                helper = self.renting.functions.getBikeState(self.bikeId).call()
                if(helper[1] == self.account.address):
                        self.available = (helper[5] == '0x0000000000000000000000000000000000000000')
                        logging.info("I am available: %d", self.available)
                        lat_at_blockchain = helper[7]
                        lon_at_blockchain = helper[8]
                        self._check_position(lat_at_blockchain, lon_at_blockchain)
                        if(helper[9]):
                                self._bike_return()
                        elif (self.available):
                                self.lock.close()
                        else:
                                self.lock.open()

        def _get_bike_id(self):
                helper = self.renting.functions.getMyBikeId().call({'from': self.account.address})
                logging.info("existence %d", helper[0])
                if(helper[0]):
                        self.bikeId = helper[1]
                        logging.info("bike id is %d", self.bikeId)
                        self._get_bike_status()

        def _handle_add_bike_event(self, event):
                logging.info(event)

        def _handle_rent_bike_event(self, event):
                if(event.args['bikeAccount'] == self.account._address):
                        self.runner = Thread(target=rinkeby_connector._distance_measure, args=[self])
                        self.running = True
                        self.runner.start()
                        logging.info("started distance measuring")
                        self.lock.open()

        def _bike_return(self):
                self.running = False
                self.lock.close()
                val = self.gpsgetter.get_values()
                lat = int(float(val.get_n())*(10 ** (self.no_of_decimal_places_using_gps-2)))
                lon = int(float(val.get_e())*(10 ** (self.no_of_decimal_places_using_gps-2)))
                tx = self.renting.functions.bikeLockClosed(self.bikeId, lat, lon, int(self.distance)).buildTransaction({
                        'from': self.account._address,
                        'gas':700000,
                        'nonce':self.w3.eth.getTransactionCount(self.account._address)
                })
                signed_tx = self.w3.eth.account.sign_transaction(tx, private_key=self.private_key)
                self.w3.eth.sendRawTransaction(signed_tx.rawTransaction)
                logging.info("sent bike lock closed tx")


        def _handle_return_bike_event(self, event):
                if(event.args['bikeAccount'] == self.account._address):
                        self._bike_return()

        def _handle_remove_bike_event(self, event):
                logging.info(event)

        def start_watching_for_events(self, sleep_time):
                logging.warning('start watching for events')
                self.add_bike_filter = self.renting.events.addBikeEvent.createFilter(fromBlock="latest")
                self.rent_bike_filter = self.renting.events.rentBikeEvent.createFilter(fromBlock="latest")
                self.return_bike_filter = self.renting.events.returnBikeEvent.createFilter(fromBlock="latest")
                self.remove_bike_filter = self.renting.events.removeBikeEvent.createFilter(fromBlock="latest")
                logging.warning('created event listeners')
                while True:
                        logging.info('event polling')
                        for event in self.add_bike_filter.get_new_entries():
                                logging.info(event)
                                self._handle_add_bike_event(event)
                        for event in self.rent_bike_filter.get_new_entries():
                                logging.info(event)
                                self._handle_rent_bike_event(event)
                        for event in self.return_bike_filter.get_new_entries():
                                logging.info(event)
                                self._handle_return_bike_event(event)
                        for event in self.remove_bike_filter.get_new_entries():
                                logging.info(event)
                                self._handle_remove_bike_event(event)
                        time.sleep(sleep_time)

