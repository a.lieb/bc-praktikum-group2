pragma solidity 0.5.16;

contract Master{
  struct city {
    string country;
    string state;
    string city;
    address place;
  }

  event cityRegisteredEvent(
    string name
  );
  
  city[] public registeredCities;

  function getNoOfCities() external view returns (uint) {
    return registeredCities.length;
  }

  function getCity(uint which) external view returns (string memory){
    return string(abi.encodePacked(registeredCities[which].city, ",", registeredCities[which].state, ",", registeredCities[which].country));
  }

  function getPlace(uint which) external view returns (address) {
    return registeredCities[which].place;
  }
  
  function register(string calldata _country, string calldata _state, string calldata _city, address _place) external {
    registeredCities.push(city(_country, _state, _city, _place));
    emit cityRegisteredEvent(_city);
  }
  
}
