# bc-praktikum-group2

## How to run this project

First, deploy the master contracts

```sh
$ cd Master_Contract
$ truffle compile
$ truffle migrate
```

Save the address of the master contract.

Open City_Contract/migrations/2_deploy_contracts.js and change the parameters of the deployment of the Renting contract corresponding to your needs.
Open webgui/src/js/app.js and edit the address (in the initContract function) to the address of the just deployed master contract.

cd into the City_Contract folder

```sh
$ cd ../City_Contract
$ truffle compile
$ truffle migrate
```

cd into the webgui folder

```sh
$ cd ../webgui
$ npm run dev
```

## To add another city

remove the build folder:

```sh
$ cd City_Contract
$ rm -rf build
```

Open City_Contract/migrations/2_deploy_contracts.js and change the parameters of the deployment of the Renting contract corresponding to your needs.


run truffle:

```sh
$ truffle migrate
```

## If you change the contracts

Rember to update the abi:

```sh
$ cp City_Contract/build/contracts/Renting.json webgui/src/city/Renting.json
$ cp Master_Contract/build/contracts/Master.json webgui/src/Master.json
```

Group Members:
Lennard Rüdesheim
Alexander Lieb
