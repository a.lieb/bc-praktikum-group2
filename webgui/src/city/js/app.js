App = {
    web3Provider: null,
    contracts: {},
    account: '0x0',
    hasVoted: false,
    lastRender: 0,
	render: 0,
	map: new OpenLayers.Map("mapdiv"),
	markerLayer: new OpenLayers.Layer.Markers( "Markers" ),
    
    init: function() {
	return App.initWeb3();
    },
    
    initWeb3: function() {
	// TODO: refactor conditional
	if (typeof web3 !== 'undefined') {
	    // If a web3 instance is already provided by Meta Mask.
	    App.web3Provider = web3.currentProvider;
	    web3 = new Web3(web3.currentProvider);
	} else {
	    // Specify default instance if no web3 instance provided
	    App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
	    web3 = new Web3(App.web3Provider);
	}
	return App.initContract();
    },
    
    initContract: function() {
	$.getJSON("Renting.json", function(renting) {
	    // Instantiate a new truffle contract from the artifact
	    //App.contracts.Renting = TruffleContract(renting);
	    
	    var contract = web3.eth.contract(renting.abi);
	    
	    var loc = location.href;
	    
	    var place = loc.search("cityaddress=");
	    
	    var address = loc.substring(place + "cityaddress=".length);
	    
	    App.contracts.Renting = contract.at(address);
	    
	    App.listenForEvents();

	    return App.render();
	});
    },
    
    // Listen for events emitted from the contract
    listenForEvents: function() {
	
	var addBikeEvt = App.contracts.Renting.addBikeEvent({}, {fromBlock: 0, toBlock: 'latest'});
	var rentBikeEvt = App.contracts.Renting.rentBikeEvent({}, {fromBlock: 0, toBlock: 'latest'});
	var returnBikeEvt = App.contracts.Renting.returnBikeEvent({}, {fromBlock: 0, toBlock: 'latest'});
	var removeBikeEvt = App.contracts.Renting.removeBikeEvent({}, {fromBlock: 0, toBlock: 'latest'});
	var refundEvt = App.contracts.Renting.refundEvent({}, {fromBlock: 0, toBlock: 'latest'});
	var correctLocationEvt = App.contracts.Renting.correctLocationEvent({}, {fromBlock: 0, toBlock: 'latest'});
	
	addBikeEvt.watch(function(error, event) {
        console.log("add bike event triggered", event);
	    App.render();
	});
	
	rentBikeEvt.watch(function(error, event) {
        console.log("rent bike event triggered", event);
	    App.render();
	});

	returnBikeEvt.watch(function(error, event) {
	    console.log("return bike event triggered", event);
	    App.render();
	});
	
	removeBikeEvt.watch(function(error, event) {
	    console.log("remove bike event triggered", event);
	    App.render();
	});

	refundEvt.watch(function(error, event) {
	    console.log("refund event triggered", event);
	    App.render();
	});

	correctLocationEvt.watch(function(error, event) {
	    console.log("correctLocation event triggered ", event);
	    App.render();
	});
	
	
	},

	addMarker:function(lon_deg, lon_min, lon_sec, lon_dez ,lat_deg, lat_min, lat_sec, lat_dez) {
		var lat_dd = lat_deg + lat_min/60 + lat_sec/(60*60) + lat_dez/(60*60*60);
		var lon_dd = lon_deg + lon_min/60 + lon_sec/(60*60) + lon_dez/(60*60*60);
		App.map.addLayer(new OpenLayers.Layer.OSM());
		var lonLat = new OpenLayers.LonLat(lon_dd, lat_dd)
			  .transform(
				new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
				App.map.getProjectionObject() // to Spherical Mercator Projection
			  );
		var zoom=11;
		App.markerLayer.addMarker(new OpenLayers.Marker(lonLat));

		App.map.setCenter (lonLat, zoom);
	},

    helper: function(i) {
	var bikesList = $("#bikes");
	var availableBikes = $("#availableBikes");
	var addedBikes = $("#addedBikes");
	var rentedBikes = $("#rentedBikes");
	var ownerForm = $("#ownerForm");
	var renterForm = $("#renterForm");
	var rentForm = $("#rentForm");
	
	App.contracts.Renting.bikes(i, {}, function(error, bike) {
	    var id = i;
	    var valid = (bike[0] != 0);
		var owner = bike[0];
		var bike_account = bike[1];
      	var hirer_refund = bike[2];
		var base_price = bike[3];
		var cpkm = bike[4];
	    var rented = (bike[5] != 0);
		var hirer = bike[5]; 
		var amount_hirer_paid = bike[6];
		var lat = bike[7];
		var long = bike[8];
		var started_return = bike[9];
		var renting_time = bike[10];
	    
	    // Render candidate Result
	    var bikeTemplate =
		"<tr><th>" +
		id +  
		"</th><th>" +
		owner +
		"</th><td>" +
		hirer_refund +
		"</td><td>" +
		base_price +
		"</td><td>" +
		cpkm +
		"</td><td>" +
		(!rented && !started_return) +
               /*   "</td><td>" +
                bike_account + */
                "</td><td>" +
                lat +
                "</td><td>" +
				long + 
				"</td><td>" +
				renting_time.toNumber();
	    "</td></tr>";
	    if(valid){
		bikesList.append(bikeTemplate);
	    }
	    // Render available bikes
	    var bikeOption =
		"<option value='" + [id, base_price, hirer_refund] + "' >" + id + "</option>";
	    if(rented != true && valid && started_return != true){
			availableBikes.append(bikeOption);
			rentForm.show();
	    }
	    
	    var addedBikeOption =
		"<option value ='" + id + "'>" + id +"</option>";
	    if(owner == App.account && rented != true){
		addedBikes.append(addedBikeOption);
		ownerForm.show();
	    }
	    
	    var rentedBikesOption =
		"<option value ='" + id + "'>" + id +"</option>";
	    if(hirer == App.account && started_return != true){
		rentedBikes.append(rentedBikesOption);
		rentedBikes.show();
		renterForm.show();
		}

		if(rented != true && valid && started_return != true && long.toNumber() != 0){
			var latString = lat.toString();
			var lat_deg = "";
			var lat_min = "";
			var lat_sec = "";
			var lat_dez = "";
			var j = latString.length;
			while (j--) {
				if (latString.length - j >= 7){
					lat_deg = latString[j] + lat_deg;
				}else if (latString.length - j >= 5){
					lat_min = latString[j] + lat_min;
				}else if (latString.length - j >= 3){
					lat_sec = latString[j] + lat_sec;
				}else {
					lat_dez = latString[j] + lat_dez;
				}
			}

			var lonString = long.toString();
			var lon_deg = "";
			var lon_min = "";
			var lon_sec = "";
			var lon_dez = "";
			var j = lonString.length;
			while (j--) {
				if (lonString.length - j >= 7){
					lon_deg = lonString[j] + lon_deg;
				}else if (lonString.length - j >= 5){
					lon_min = lonString[j] + lon_min;
				}else if (lonString.length - j >= 3){ 
					lon_sec = lonString[j] + lon_sec;
				}else{
					lon_dez = lonString[j] + lon_dez;
				}
			}
			App.addMarker(parseInt(lon_deg), parseInt(lon_min),parseInt(lon_sec),parseInt(lon_dez), 
					parseInt(lat_deg),parseInt(lat_min), parseInt(lat_sec),parseInt(lat_dez));
		}


	});
	},
    
    render: function() {

	var now = new Date();
	if((now.getTime() - App.lastRender) < 2000) return;

	App.lastRender = now.getTime();
	
      var electionInstance;
      var loader = $("#loader");
      var content = $("#content");
	  var addForm = $("#addForm");
	  var rentForm = $("#rentForm");
      var ownerForm = $("#ownerForm");
      var renterForm = $("#renterForm");
	  var refundForm = $("#refundForm");

      loader.show();
      content.hide();
	  ownerForm.hide();
	  addForm.hide();
	  rentForm.hide();
      renterForm.hide();
	  refundForm.hide();
	  
      
      // Load account data
      web3.eth.getCoinbase(function(err, account) {
	  if (err === null) {
              App.account = account;
			  $("#accountAddress").html("Your Account: " + account);
			  
			  //check if refund for current account is available
			  App.contracts.Renting.refunds(App.account, {}, function(error, result) {
			  var refundValue = $("#refundValues");
			  refundValue.empty();
			  if(result.toNumber() > 0){
				  refundValue.append("<option value='' >" + result.toNumber() +"</option>");
				  refundForm.show();
			  } else {
				  refundForm.hide();
			  }
			  });
	  }
	  });
	  
      var bikeCount = App.contracts.Renting.bikeCount({}, function(error, result) {
	  
	  var bikesList = $("#bikes");
	  bikesList.empty();
	  
	  var availableBikes = $("#availableBikes");
	  availableBikes.empty();
	  
	  var addedBikes = $("#addedBikes");
	  addedBikes.empty();
	      
	  var rentedBikes = $("#rentedBikes");
	  rentedBikes.empty(); 
	
	  // remove all markers
	  App.markerLayer.destroy();
	  App.markerLayer = new OpenLayers.Layer.Markers( "Markers" );
	  App.map.addLayer(App.markerLayer);

	  // Store all prosed to get bikes
	  var i;
	  for (i = 1; i <= result.c[0]; i++) {
	      App.helper(i);
	  }    
	  });

	loader.hide();
	content.show();
	addForm.show();

    },
    
    rentBike: function() {
	var IdPriceArray = $('#availableBikes').val().split(',');
	var price = IdPriceArray[1] + IdPriceArray[2]; // base_price + hirer_refund
	App.contracts.Renting.rentBike(IdPriceArray[0], {from: App.account, value: price}, function(error, result) {
		$("#content").hide();
		$("#ownerForm").hide();
		$("#rentedBikes").hide();
		$("#addForm").hide();
		$("#rentForm").hide();
		$("#renterForm").hide();
		$("#refundForm").hide();
		$("#loader").show();
	
		
		//alert
		$('#rentBike_alert').show()
	    $('#rentBike_alert .close').click(function(e) {
		$("#rentBike_alert").hide();
	    });
	});
    },
    
    addBike: function() {
	var costs = $('#priceSelect').val();
	var fund;
	var wait = false;
	var hirerFund = $('#deposit').val();
	var bikeAccount = $('#bikeAccount').val();
	var lat = $('#lat').val();
	var long = $('#long').val();
	var cpkm = $('#cpkm').val();
	App.contracts.Renting.owner_refund({}, function(error, fund) {
	    App.contracts.Renting.addBike(costs, cpkm, hirerFund, bikeAccount, lat, long, {from: App.account, value: Number(fund)+100}, function(error, result) {
			$("#content").hide();
			$("#ownerForm").hide();
			$("#rentedBikes").hide();
			$("#addForm").hide();
			$("#rentForm").hide();
			$("#renterForm").hide();
			$("#refundForm").hide();
			$("#loader").show();
	

		//alert
		$('#addBike_alert').show()
		$('#addBike_alert .close').click(function(e) {
		    $("#addBike_alert").hide();
		});
	    });
	});
    },

    removeBike: function() {
	var bikeId = $('#addedBikes').val();

	App.contracts.Renting.removeBike(bikeId, {from: App.account}, function(error, result) {
		$("#content").hide();
		$("#ownerForm").hide();
		$("#rentedBikes").hide();
		$("#addForm").hide();
		$("#rentForm").hide();
		$("#renterForm").hide();
		$("#refundForm").hide();
		$("#loader").show();

		
		//alert 
		$('#removeBike_alert').show()
	    $('#removeBike_alert .close').click(function(e) {
		$("#removeBike_alert").hide();
	    });
	});
    },

    returnBike: function() {
	var bikeId = $('#rentedBikes').val();

	App.contracts.Renting.returnBike(bikeId, {from: App.account}, function(error, result) {
		$("#content").hide();
		$("#ownerForm").hide();
		$("#rentedBikes").hide();
		$("#addForm").hide();
		$("#rentForm").hide();
		$("#renterForm").hide();
		$("#refundForm").hide();
		$("#loader").show();

		
		//alert
		$('#returnBike_alert').show()
	    $('#returnBike_alert .close').click(function(e) {
		$("#returnBike_alert").hide();
	    });
	});
    },

    refund: function() {
	App.contracts.Renting.refund(0,{from: App.account}, function(error, result) {
		$("#content").hide();
		$("#ownerForm").hide();
		$("#rentedBikes").hide();
		$("#addForm").hide();
		$("#rentForm").hide();
		$("#renterForm").hide();
		$("#refundForm").hide();
		$("#loader").show();


		//alert
	    $('#refund_alert').show()
	    $('#refund_alert .close').click(function(e) {
		$("#refund_alert").hide();
	    });
	});
	},
	
	backToCitySelection: function() {
		var url = window.location.origin;
		console.log(url);
	    window.open(url, "_self");
		}

	
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
