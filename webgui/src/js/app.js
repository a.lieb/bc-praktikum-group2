App = {
    web3Provider: null,
    contracts: {},
    account: '0x0',
    hasVoted: false,
    lastRender: 0,
    
    init: function() {
	return App.initWeb3();
    },
    
    initWeb3: function() {
	if (typeof web3 !== 'undefined') {
	    // If a web3 instance is already provided by Meta Mask.
	    App.web3Provider = web3.currentProvider;
	    web3 = new Web3(web3.currentProvider);
	} else {
	    // Specify default instance if no web3 instance provided
	    App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
	    web3 = new Web3(App.web3Provider);
	}
	return App.initContract();
    },
    
    initContract: function() {
	$.getJSON("Master.json", function(master) {
	    // Instantiate a new truffle contract from the artifact
	    //App.contracts.Master = TruffleContract(master);

	    var contract = web3.eth.contract(master.abi);	    

	    App.contracts.Master = contract.at("0xB411c9F634C77FCFa38842000486F4F19583D6e2");
	    
	    App.listenForEvents();

	    return App.render();
	});
    },
    
    
    listenForEvents: function() {
	var registerEvt = App.contracts.Master.cityRegisteredEvent({}, {fromBlock: 0, toBlock: 'latest'});
	registerEvt.watch(function(error, event) {
	    console.log("event received: ", event);
	    App.render();
	});
    },

    helper: function(i) {
	App.contracts.Master.getCity(i, {}, function(error, result) {
		var cityValue = $("#City-Select");
	    cityValue.append("<option value='" + i +  "' >" + result + "</option>");
	});
    },
    
    render: function() {

	var now = new Date();

	if((now.getTime() - App.lastRender) < 2000) return;

	App.lastRender = now.getTime();

	var loader = $("#loader");
	var forms = $("#forms");

	var cityValue = $("#City-Select");
	cityValue.empty();
	
	loader.show();
	// Load account data
	web3.eth.getCoinbase(function(err, account) {
	    if (err === null) {
		App.account = account;
		$("#accountAddress").html("Your Account: " + account);
	    }
	});
			
	App.contracts.Master.getNoOfCities({}, function(error, result) {
	    console.log("nof of cities is", result.toNumber());
	    var cityValue = $("#City-Select");
	    cityValue.empty();
	    var i;
	    for(i = 0; i < result; ++i) {
		App.helper(i);
	    }
	});
	    
	loader.hide();
	forms.show();
    },
	
    chooseCity: function() {
	var value = $("#City-Select").val();
	App.contracts.Master.getPlace(value , {}, function(error, result) {
	    var url = location.href + "city/index.html?cityaddress=" + result;
	    window.open(url, "_self");
	});
	}
	
};
    

$(function() {
    $(window).load(function() {
	App.init();
    });
});
